(() => {
    type UserID = number | string;
    let userId: UserID;

    //Literal types
    type Sizes = 'S' | 'M' | 'L' | 'XL';
    let shirtSize: Sizes;
    shirtSize = 'S';
    shirtSize = 'L';

    function greetings(myVar: UserID, size: Sizes) {
        if (typeof myVar === 'string')
            console.log(`String: ${myVar.toUpperCase()}`);
    }

    greetings('AB123', 'S');
})();
