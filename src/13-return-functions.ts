(() => {
    const calcTotal = (prices: number[]): string => {
        let total = prices.reduce((prev, current) => prev + current, 0);
        return total.toString(); //Por que nos pide retornar un string
    };

    const rta = calcTotal([1, 2, 3, 4, 5]);
    console.log(rta);
})();
