(() => {
    let userId: number | string;
    userId = 123;
    userId = 'Hola amigo';

    function greetings(myVar: number | string) {
        typeof myVar === 'string'
            ? console.log(`String: ${myVar.toUpperCase()}`)
            : console.log(`Number: ${myVar.toFixed(2)}`);
    }

    greetings('John');
    greetings(21.432653);
})();
