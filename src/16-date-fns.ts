import { format, subDays } from 'date-fns';

const date = new Date(2014, 1, 15); //0=enero, 1=febrero
let rta = subDays(date, 30);
let str = format(rta, 'yyyy/MMM/dd');
console.log(str);
