(() => {
    let myNumber: number | null = null;
    myNumber = 12;
    let myString: string | undefined = undefined;
    myString = 'sda';

    //function hi(name: string | null) {
    //    let hello: string = 'Hola ';
    //    name ? (hello += name) : (hello += 'nobody');
    //    console.log(hello);
    //
    //}
    function hiV2(name: string | null) {
        let hello: string = 'Hola ';
        hello += name?.toUpperCase() ?? 'nobody';
        console.log(hello);
    }

    hiV2('John');
    hiV2(null);
})();
