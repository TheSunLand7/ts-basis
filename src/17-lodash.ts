import { addMinutes } from 'date-fns';
import _ from 'lodash';

let data: { username: string; role: string }[] = [
    {
        username: 'john',
        role: 'admin',
    },
    {
        username: 'solange',
        role: 'seller',
    },
    {
        username: 'magnus',
        role: 'seller',
    },
    {
        username: 'susan',
        role: 'customer',
    },
];

const rta = _.groupBy(data, (item) => item.role);
console.log(rta);
