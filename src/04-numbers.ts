(() => {
    let price = 100;
    console.log('Price: ', price);

    let customerAge: number = 21;
    customerAge = customerAge + 1;

    console.log('customerAge: ', customerAge);

    let product: number; //Cuando se quiere declarar pero no asignar una variable se debe colocar el tipo de dato explícitamente.


    let discount = +'123';
    console.log('discount: ', discount);
    
    let hex = 0xfff;
    console.log('hex', hex);
    let bin = 0b1010;
    console.log('bin', bin);
    
})();