(() => {
    type Sizes = 'S' | 'M' | 'L' | 'XL';
    type Data = {
        title: string;
        createdAt: Date;
        stock: number;
        size?: Sizes;
    };
    let products: any[] = [];
    const addProduct = (data: Data) => {
        //const {title,createdAt,stock,size} = data;
        products.push(data);
    };

    addProduct({
        title: 'Pro1',
        createdAt: new Date(2018, 2, 5),
        stock: 12,
    });

    addProduct({
        title: 'Pro1',
        createdAt: new Date(2018, 2, 5),
        stock: 12,
        size: 'M',
    });
    console.log(products);
})();
