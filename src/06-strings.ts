(() => {
    let productTitle: string = 'My amazing product';
    let productDescription: string = 'All I ever hear from you is bla bla bla';
    let price:number = 100;
    let isNew: boolean = false;

    let summary = `
        title: ${productTitle}
        description: ${productDescription}
        price: $${price}
        new: ${isNew}
    `;
    console.log(summary);
    
})();