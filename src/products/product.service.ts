import { Product } from './product.model';

let products: Product[] = [];
const addProduct = (data: Product) => {
    //const {title,createdAt,stock,size} = data;
    products.push(data);
};

const calcStock = (): number => {
    let total = 0;
    products.forEach((item) => {
        total += item.stock;
    });
    return total;
};
export { products, addProduct, calcStock };
