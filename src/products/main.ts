import { calcStock, addProduct, products } from './product.service';

addProduct({
    title: 'Pro1',
    createdAt: new Date(2018, 2, 5),
    stock: 5,
});

addProduct({
    title: 'Pro1',
    createdAt: new Date(2018, 2, 5),
    stock: 6,
    size: 'M',
});

console.log(calcStock());
