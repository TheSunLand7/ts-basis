(() => {
    let myDinamicVar: any;
    myDinamicVar = 123;
    myDinamicVar = true;
    myDinamicVar = 'Hola';

    let saludo = (myDinamicVar as string); //'as' nos permite convertir any al tipo de dato que queremos
    console.log(saludo.toUpperCase());
    
    myDinamicVar = 123.2218476523;
    let rta = (<number>myDinamicVar).toFixed(2);
    console.log(rta);
})();